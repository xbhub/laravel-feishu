<?php
/**
 * Created by PhpStorm.
 * User: win10
 * Date: 2018/5/16
 * Time: 17:09
 */

Route::group([
    'prefix'=>'callback',
    'middleware' => ['web'],
    'as' => 'callback.',
    'namespace'=>'Xbhub\Feishu\Http\Controllers'
], function(){

    Route::post('/feishu', 'FeishuController@callback')->name('feishu');
});
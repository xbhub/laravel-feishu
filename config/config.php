<?php

return [
    'app_id'     => env('FEISHU_APP_ID'),
    'app_secret' => env('FEISHU_APP_SECRET'),
    'encrypt_key'=> env('FEISHU_ENCRYPT_KEY'),
];

<?php

namespace Xbhub\Feishu;

use Illuminate\Support\Facades\Facade;

class Feishu extends Facade
{

    public static function getFacadeAccessor()
    {
        return 'feishu';
    }

    public static function __callStatic($name, $args)
    {
        return app('feishu')->$name;
    }
}

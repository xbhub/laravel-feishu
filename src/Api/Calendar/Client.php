<?php declare(strict_types=1);

/*
 * This file is part of the xbhub/feishu.
 *
 * (c) jory <jorycn@163.com>
 *
 * This source file is subject to the MIT license that is bundled
 * with this source code in the file LICENSE.
 */

namespace Xbhub\Feishu\Api\Calendar;

use Xbhub\Feishu\Api\Kernel\BaseClient;
use Illuminate\Support\Facades\Log;

/**
 * Class Client.
 *
 * @author jory <jorycn@163.com>
 */
class Client extends BaseClient
{

    /**
     * 创建日历
     *
     * @param string $summary 日历标题，最大长度为 256
     * @param string $description 日历描述，最大长度为 256
     * @param boolean $is_private 是否为私有日历，私有日历不可被搜索，默认为fals
     * @param string $default_access_role reader|free_busy_reader: 订阅者,可查看日程详情|: 游客，只能看到"忙碌/空闲"
     * @return void
     */
    public function create(string $summary, string $description = '', bool $is_private = false, string $default_access_role = 'reader')
    {
        return $this->httpPostJson('calendar/v3/calendars', [
            'summary' => $summary,
            'description' => $description,
            'is_private' => $is_private,
            'default_access_role' => $default_access_role
        ]);
    }

    /**
     * 获取日历列表
     *
     * @param integer $max_results
     * @param string $page_token
     * @param string $sync_token
     * @return void
     */
    public function list(int $max_results = 500, string $page_token = '', string $sync_token = '')
    {
        return $this->httpGet('calendar/v3/calendar_list', [
            'max_results' => $max_results,
            'page_token' => $page_token,
            'sync_token' => $sync_token
        ]);
    }

    /**
     * 删除日历
     *
     * @param string $calendarId
     * @return void
     */
    public function delete(string $calendarId)
    {
        return $this->httpDelete('calendar/v3/calendars/'.$calendarId);
        
    }

    /**
     * 日程创建
     *
     * @param string $calendarId
     * @param string $summary
     * @param integer $start_at
     * @param integer $end_at
     * @param array $options
     *      - description 日程描述，最大长度为 256，默认空
     *      - attendees 日程参与者信息，默认空数组 每个 Attendess 必须有 open_id 或者 employee_id ['open_id','employee_id','display_name']
     *      - visibility 公开范围。取值如下：default: 默认，仅向他人显示是否“忙碌”public：公开，显示日程详情private：仅自己可见默认 "default"
     * @return void
     */
    public function eventCreate(string $calendarId, string $summary, int $start_at, int $end_at, array $options = [])
    {
        return $this->httpPostJson('calendar/v3/calendars/'.$calendarId.'/events', [
            'summary' => $summary,
            'start' => ['time_stamp' => $start_at, 'time_zone' => 'Asia/Shanghai'],
            'end' => ['time_stamp' => $end_at, 'time_zone' => 'Asia/Shanghai'],
            'attendees' => $options['attendees'] ?? [],
            'description' => $options['description'] ?? '',
            'visibility' => $options['visibility'] ?? 'default'
        ]);
    }

    /**
     * get calendar detail
     *
     * @param string $calendarId
     * @return void
     */
    public function get(string $calendarId)
    {
        return $this->httpGet('calendar/v3/calendar_list/' . $calendarId);
    }


    public function eventDelete(string $calendarId, string $eventId)
    {
        return $this->httpDelete('calendar/v3/calendars/'.$calendarId.'/events/'.$eventId);
    }


}

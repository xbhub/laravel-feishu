<?php declare(strict_types=1);

/*
 * This file is part of the xbhub/feishu.
 *
 * (c) jory <jorycn@163.com>
 *
 * This source file is subject to the MIT license that is bundled
 * with this source code in the file LICENSE.
 */

namespace Xbhub\Feishu\Api\Role;

use Xbhub\Feishu\Api\Kernel\BaseClient;
use Illuminate\Support\Facades\Log;

/**
 * Class Client.
 *
 * @author jory <jorycn@163.com>
 */
class Client extends BaseClient
{

    /**
     * [code description]
     * @return [type] [description]
     */
    public function list()
    {
        return $this->httpGet('contact/v2/role/list');
    }

    public function members(string $role_id, string $page_token, int $page_size)
    {
        return $this->httpGet('contact/v2/role/members', [
            'role_id' => $role_id,
            'page_token' => $page_token,
            'page_size' => $page_size
        ]);
    }


}

<?php

/*
 * This file is part of the xbhub/feishu.
 *
 * (c) jory <jorycn@163.com>
 *
 * This source file is subject to the MIT license that is bundled
 * with this source code in the file LICENSE.
 */

namespace Xbhub\Feishu\Api\User;

use Xbhub\Feishu\Api\Kernel\BaseClient;
use Illuminate\Support\Facades\Log;

/**
 * Class Client.
 *
 * @author jory <jorycn@163.com>
 */
class Client extends BaseClient
{
    /**
     * @param int   $departmentId
     * @param int   $size
     * @param int   $offset
     * @param array $params
     *
     * @return array
     */
    public function detaillist(bool $fetch_child=false, string $departmentId = '0', int $offset = 0, int $size = 100) {
        return $this->httpGet('contact/v1/department/user/detail/list', [
            'department_id' => $departmentId,
            'offset'        => $offset,
            'page_size'     => $size,
            'fetch_child'   => $fetch_child
        ]);
    }

    /**
     * @param string $type userid | openid
     * @param array $ids
     * @return void
     */
    public function batch_get(string $type, array $ids)
    {
        $params = [];
        if($type == 'userid') $params['employee_ids'] = implode(',', $ids);
        if($type == 'openid') $params['open_ids'] = implode(',', $ids);

        return $this->httpGet('contact/v1/user/batch_get', $params);
    }
}

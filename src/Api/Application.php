<?php

/*
 * This file is part of the xbhub/feishu.
 *
 * (c) jory <jorycn@163.com>
 *
 * This source file is subject to the MIT license that is bundled
 * with this source code in the file LICENSE.
 */

namespace Xbhub\Feishu\Api;

use Pimple\Container;

/**
 * Class Application.
 *
 * @author jory <jorycn@163.com>
 *
 * @property \namespace Xbhub\Feishu\Api\Auth\Client $auth
 * @property \namespace Xbhub\Feishu\Api\Chat\Client $chat
 * @property \namespace Xbhub\Feishu\Api\Role\Client $role
 * @property \namespace Xbhub\Feishu\Api\User\Client $user
 * @property \namespace Xbhub\Feishu\Api\Media\Client $media
 * @property \namespace Xbhub\Feishu\Api\Jssdk\Client $jssdk
 * @property \namespace Xbhub\Feishu\Api\Checkin\Client $checkin
 * @property \namespace Xbhub\Feishu\Api\Message\Client $message
 * @property \namespace Xbhub\Feishu\Api\Attendance\Client $attendance
 * @property \namespace Xbhub\Feishu\Api\Kernel\Credential $credential
 * @property \namespace Xbhub\Feishu\Api\Department\Client $department
 * @property \namespace Xbhub\Feishu\Api\Message\AsyncClient $async_message
 */
class Application extends Container
{
    /**
     * @var array
     */
    protected $providers = [
        Kernel\ServiceProvider::class, // 核心
        User\ServiceProvider::class, // 人员
        Department\ServiceProvider::class, // 部门
        Role\ServiceProvider::class, // 角色
        Message\ServiceProvider::class, //机器人消息
        Process\ServiceProvider::class,
        Calendar\ServiceProvider::class,
    ];

    /**
     * Application constructor.
     *
     * @param array $config
     */
    public function __construct(array $config)
    {
        parent::__construct();

        $this['config'] = function () use ($config) {
            return new Kernel\Config($config);
        };

        $this->registerProviders();
    }

    /**
     * Register providers.
     */
    protected function registerProviders()
    {
        foreach ($this->providers as $provider) {
            $this->register(new $provider());
        }
    }

    /**
     * @param $id
     *
     * @return mixed
     */
    public function __get($id)
    {
        return $this->offsetGet($id);
    }
}

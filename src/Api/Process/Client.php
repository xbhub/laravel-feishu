<?php


namespace Xbhub\Feishu\Api\Process;

use Xbhub\Feishu\Api\Kernel\BaseClient;
use Xbhub\Feishu\Api\Kernel\Exceptions\ClientError;
use Illuminate\Support\Str;

/**
 * Class Client.
 *
 * @author jory <jorycn@163.com>
 */
class Client extends BaseClient
{
    const APPROVAL_HOST = 'https://www.feishu.cn/';

    /**
     * @param $userid
     * @param int $offset
     * @param int $size
     * @return array|\GuzzleHttp\Psr7\Response
     */
    public function get(string $approval_code)
    {
        return $this->httpPostJson(self::APPROVAL_HOST . 'approval/openapi/v2/approval/get', [
            'approval_code' => $approval_code,
            'locale' => 'zh-CN'
        ]);
    }

    /**
     * @param string $approval_code
     * @param string $user_id
     * @param string $department_id
     * @param array $form
     * @param array $node_approver_user_id_list
     * @return void
     */
    public function create(string $approval_code, string $user_id, string $department_id, array $form, array $options = [])
    {
        return $this->httpPostJson(self::APPROVAL_HOST . 'approval/openapi/v2/instance/create', [
            'approval_code' => $approval_code,
            'user_id' => $user_id,
            'department_id' =>$department_id,
            'form' => json_encode($form),
            'node_approver_user_id_list' => $options['node_approver_user_id_list'] ?? [],
            'uuid' => md5($form)
        ]);
    }

    /**
     * @param string $approval_code
     * @param integer $start_time （毫秒）
     * @param integer $end_time （毫秒）
     * @param integer $offset
     * @param integer $limit
     * @return void
     */
    public function instanceList(string $approval_code, int $start_time, int $end_time, int $offset = 0, int $limit = 100)
    {
        return $this->httpPostJson(self::APPROVAL_HOST . 'approval/openapi/v2/instance/list', [
            'approval_code' => $approval_code,
            'start_time' => $start_time,
            'end_time' =>$end_time,
            'offset' => $offset,
            'limit' => $limit
        ]);
    }

    /**
     * 审批实例获取
     * @param string $instance_code
     * @param string $locale
     * @return void
     */
    public function instanceGet(string $instance_code, string $locale = 'zh-CN')
    {
        return $this->httpPostJson(self::APPROVAL_HOST . 'approval/openapi/v2/instance/get', [
            'instance_code' => $instance_code,
            'locale' => $locale
        ]);
    }


    /**
     * 审批事件订阅
     *
     * @param string $approval_code
     * @return void
     */
    public function subscribe(string $approval_code)
    {
        return $this->httpPostJson(self::APPROVAL_HOST . 'approval/openapi/v2/subscription/subscribe', [
            'approval_code' => $approval_code
        ]);
    }

    /**
     * 审批取消订阅
     *
     * @param string $approval_code
     * @return void
     */
    public function unsubscribe(string $approval_code)
    {
        return $this->httpPostJson(self::APPROVAL_HOST . 'approval/openapi/v2/subscription/unsubscribe', [
            'approval_code' => $approval_code
        ]);
    }

    
    /**
     * 附件上传
     *
     * @param integer $type
     * @param string $name
     * @param string $path
     * @return void
     */
    public function fileupload(int $type, string $name, string $path)
    {
        $typeMap = ['1' => 'image', '2' => 'attachment'];
        return $this->httpUpload(
            self::APPROVAL_HOST . 'approval/openapi/v2/file/upload', 
            [$name => $path], 
            [
                'name' => $name,
                'type' => $typeMap[$type] ?? 'attachment'
            ]
        );
    }

}

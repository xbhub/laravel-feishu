<?php

/*
 * This file is part of the xbhub/feishu.
 *
 * (c) jory <jorycn@163.com>
 *
 * This source file is subject to the MIT license that is bundled
 * with this source code in the file LICENSE.
 */

namespace Xbhub\Feishu\Api\Message;

use Xbhub\Feishu\Api\Kernel\BaseClient;
use Illuminate\Support\Facades\Log;

/**
 * Class Client.
 *
 * @author jory <jorycn@163.com>
 */
class Client extends BaseClient
{

    /**
     * [code description]
     * @return [type] [description]
     */
    public function batch_send(array $owner, string $type,  $content)
    {
        return $this->httpPostJson('message/v4/batch_send/', [
            'department_ids' => $owner['department_ids'] ?? [],
            'open_ids' => $owner['open_ids'] ?? [],
            'user_ids' => $owner['user_ids'] ?? [],
            'msg_type' => $type,
            'content' => [$type => $content]
        ]);
    }


    /**
     * @param $uid   chat_id > open_id > user_id > email
     * @param $type
     * @param $content
     * @param $root_id
     * @return array|\GuzzleHttp\Psr7\Response
     */
    public function send(array $uid, string $type, string $content, string $root_id = '')
    {
        $data = array_merge([
            'root_id' => $root_id,
            'msg_type' => $type,
            'content' => [$type => $content]
        ], $this->parseUid($uid));

        return $this->httpPostJson('message/v4/send/', $data);
    }

}

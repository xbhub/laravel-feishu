<?php declare(strict_types=1);

/*
 * This file is part of the xbhub/feishu.
 *
 * (c) jory <jorycn@163.com>
 *
 * This source file is subject to the MIT license that is bundled
 * with this source code in the file LICENSE.
 */

namespace Xbhub\Feishu\Api\Department;

use Xbhub\Feishu\Api\Kernel\BaseClient;
use Illuminate\Support\Facades\Log;

/**
 * Class Client.
 *
 * @author jory <jorycn@163.com>
 */
class Client extends BaseClient
{

    /**
     * [code description]
     * @return [type] [description]
     */
    public function simplelist(bool $fetch_child = false, string $department_id = '0', int $offset = 0, int $page_size = 10)
    {
        return $this->httpPostJson('contact/v1/department/simple/list', [
            'department_id' => $department_id,
            'offset' => $offset,
            'page_size' => $page_size,
            'fetch_child' => $fetch_child
        ]);
    }

    /**
     *
     * @param string $department_id
     * @return void
     */
    public function info(string $department_id)
    {
        return $this->httpGet('contact/v1/department/info/get', [
            'department_id' => $department_id
        ]);
    }


}

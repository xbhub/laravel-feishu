<?php

/*
 * This file is part of the xbhub/feishu.
 *
 * (c) jory <jorycn@163.com>
 *
 * This source file is subject to the MIT license that is bundled
 * with this source code in the file LICENSE.
 */

namespace Xbhub\Feishu\Api\Kernel;

use Xbhub\Feishu\Api\Application;
use GuzzleHttp\HandlerStack;
use GuzzleHttp\RequestOptions;
use Psr\Http\Message\RequestInterface;
use GuzzleHttp\Middleware;

/**
 * Class BaseClient.
 *
 * @author jory <jorycn@163.com>
 */
class BaseClient
{
    use MakesHttpRequests;

    /**
     * @var \namespace Xbhub\Feishu\Api\Application
     */
    protected $app;

    protected $feishuHandlerStack;

    /**
     * @param \namespace Xbhub\Feishu\Api\Application
     */
    public function __construct(Application $app)
    {
        $this->app = $app;
    }

    /**
     * @param array $uid chat_id > open_id > user_id > email
     * @return array
     */
    public function parseUid(array $uid)
    {
        $data = [];
        isset($uid['chat_id']) ? $data['chat_id'] = $uid['chat_id'] :'';
        empty($data)&&isset($uid['open_id']) ? $data['open_id'] = $uid['open_id'] :'';
        empty($data)&&isset($uid['user_id']) ? $data['user_id'] = $uid['user_id'] :'';
        empty($data)&&isset($uid['email']) ? $data['email'] = $uid['email'] :'';

        return $data;
    }

    /**
     * Make a get request.
     *
     * @param string $uri
     * @param array  $query
     *
     * @return array|\GuzzleHttp\Psr7\Response
     */
    public function httpGet(string $uri, array $query = [])
    {
        return $this->requestFeishu('GET', $uri, [RequestOptions::QUERY => $query]);
    }

    public function httpDelete(string $uri, array $query = [])
    {
        return $this->requestFeishu("DELETE", $uri, [RequestOptions::QUERY => $query]);
    }

    /**
     * Make a post request.
     *
     * @param string $uri
     * @param array  $json
     * @param array  $query
     *
     * @return array|\GuzzleHttp\Psr7\Response
     */
    public function httpPostJson(string $uri, array $json = [], array $query = [])
    {
        return $this->requestFeishu('POST', $uri, [
            RequestOptions::QUERY => $query,
            RequestOptions::JSON  => $json,
        ]);
    }

    /**
     * Upload files.
     *
     * @param string $uri
     * @param array  $files
     * @param array  $query
     *
     * @return array|\GuzzleHttp\Psr7\Response
     */
    public function httpUpload(string $uri, array $files, array $query = [])
    {
        $multipart = [];

        foreach ($files as $name => $path) {
            $multipart[] = [
                'name'     => $name,
                'contents' => fopen($path, 'r'),
            ];
        }

        return $this->requestFeishu('POST', $uri, [
            RequestOptions::QUERY     => $query,
            RequestOptions::MULTIPART => $multipart,
        ]);
    }

    /**
     * @param $method
     * @param $uri
     * @param array $options
     *
     * @return array|\GuzzleHttp\Psr7\Response
     */
    protected function requestFeishu($method, $uri, array $options = [])
    {
        if (!$handler = $this->feishuHandlerStack) {
            $handler = HandlerStack::create();

            // 中间件追加header
            $handler->push(Middleware::mapRequest(function (RequestInterface $request) {
                return $request->withHeader('Authorization', 'Bearer '.$this->app['credential']->token());
            }));
            // 追加参数
//            $handler->push(function (callable $handler) {
//                return function (RequestInterface $request, array $options) use ($handler) {
//                    $query = [
//                        'session'    => $this->app['credential']->token(),
//                        'timestamp'  => date('Y-m-d H:i:s'),
//                        'format'     => 'json',
//                        'v'          => '2.0',
//                        'partner_id' => null,
//                        'simplify'   => 'true',
//                    ];
//
//                    return $handler($this->concat($request, $query), $options);
//                };
//            });

            $this->feishuHandlerStack = $handler;
        }

        return $this->request($method, $uri, $options + compact('handler'));
    }

    /**
     * @param \Psr\Http\Message\RequestInterface $request
     * @param array                              $query
     *
     * @return \Psr\Http\Message\RequestInterface
     */
    protected function concat(RequestInterface $request, array $query = []): RequestInterface
    {
        parse_str($request->getUri()->getQuery(), $parsed);
        $query = http_build_query(array_merge($query, $parsed));

        return $request->withUri($request->getUri()->withQuery($query));
    }
}

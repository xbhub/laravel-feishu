<?php

/*
 * This file is part of the xbhub/feishu.
 *
 * (c) jory <jorycn@163.com>
 *
 * This source file is subject to the MIT license that is bundled
 * with this source code in the file LICENSE.
 */

namespace Xbhub\Feishu\Api\Kernel;

use Xbhub\Feishu\Api\Application;

/**
 * Class Credential.
 *
 * @author jory <jorycn@163.com>
 */
class Credential
{
    use MakesHttpRequests;

    /**
     * @var \namespace Xbhub\Feishu\Api\Application
     */
    protected $app;

    /**
     * Credential constructor.
     *
     * @param \namespace Xbhub\Feishu\Api\Application $app
     */
    public function __construct(Application $app)
    {
        $this->app = $app;
    }

    /**
     * Get credential token.
     *
     * @return string
     */
    public function token(): string
    {
        if ($value = $this->app['cache']->get($this->cacheKey())) {
            return $value;
        }

        $result = $this->request('POST', 'auth/v3/tenant_access_token/internal/', [
            'query' => $this->credentials(),
        ]);

        $this->setToken($token = $result['tenant_access_token'], 7000);

        return $token;
    }

    /**
     * @param string                 $token
     * @param int|\DateInterval|null $ttl
     *
     * @return $this
     */
    public function setToken(string $token, $ttl = null)
    {
        $this->app['cache']->set($this->cacheKey(), $token, $ttl);

        return $this;
    }

    /**
     * @return array
     */
    protected function credentials(): array
    {
        return [
            'app_id'     => $this->app['config']->get('app_id'),
            'app_secret' => $this->app['config']->get('app_secret'),
        ];
    }

    /**
     * @return string
     */
    protected function cacheKey(): string
    {
        return 'feishu.access_token.' . md5(json_encode($this->credentials()));
    }
}

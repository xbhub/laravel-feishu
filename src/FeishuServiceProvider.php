<?php

namespace Xbhub\Feishu;

use Illuminate\Support\ServiceProvider;
use Xbhub\Feishu\Api\Application;

class FeishuServiceProvider extends ServiceProvider
{
    /**
     * Perform post-registration booting of services.
     *
     * @return void
     */
    public function boot()
    {
        $this->loadRoutesFrom(__DIR__.'/../routes/route.php');
        // 发布配置
        $this->mergeConfigFrom(
            __DIR__.'/../config/config.php', 'feishu'
        );
    }

    /**
     * Register any package services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->singleton('feishu', function ($app) {
            $config = config('feishu');
            return new Application($config);
        });
    }

    public function provides()
    {
        return ['feishu'];
    }
}
<?php
namespace Xbhub\Feishu\Http\Controllers;

use Illuminate\Http\Request;
use Xbhub\Feishu\Events\Message;

use Xbhub\Feishu\Utils\Prpcrypt;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Xbhub\Feishu\Events\DeptModify;
use Xbhub\Feishu\Events\UserModify;
use Xbhub\Feishu\Events\ProcessUpdate;
use Illuminate\Support\Facades\Artisan;
use Xbhub\Feishu\Events\ApprovalUpdate;

/**
 * Class VotesController.
 *
 * @package namespace App\Http\Controllers;
 */
class FeishuController extends Controller
{

    /**
     * @throws \Xbhub\Feishu\Utils\Exception
     */
    public function callback()
    {
        try {
            $_config = config('feishu');
            $prpcrypt = new Prpcrypt($_config['encrypt_key']);

            $res = $prpcrypt->decrypt(\request()->get('encrypt'));
        } catch (Exception $e) {
            Log::error("DecryptMsg Exception".$e->getMessage());
            print $e->getMessage();
            exit();
        }
        
        Log::debug("【callback】:".$res);
        $res = json_decode($res, true);

        if(isset($res['challenge'])) {
            // 接口探测
            return response()->json(['challenge' => $res['challenge']]);
        }else if(isset($res['type']) && $res['type']=='event_callback'){
            // 事件回调
            if(!isset($res['event']) || !isset($res['event']['type'])) {
                Log::debug('未知飞书事件：'.json_encode($res));
                return;
            }

            switch($res['event']['type']){
                case 'message':
                    // 机器人消息
                    event(new Message($res['event']));
                    // Log::debug("【callback】:Message");
                    break;
                case 'user_add':
                case 'user_update':
                case 'user_leave':
                    // 人员变更
                    event(new UserModify($res['event']));
                    // Log::debug("【callback】:UserModify");
                    break;
                case 'dept_add':
                case 'dept_update':
                case 'dept_delete':
                    event(new DeptModify($res['event']));
                    break;
                case 'approval':
                case 'approval_instance':
                case 'approval_task':
                case 'approval_cc':
                    // 审批
                    event(new ApprovalUpdate($res['event']));
                    // Log::debug("【callback】:ApprovalUpdate");
                    break;
            }
        }

        return;
    }

}

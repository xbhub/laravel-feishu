import hashlib
import base64
from Crypto.Cipher import AES
class  AESCipher(object):
    def __init__(self, key):
        self.bs = AES.block_size
        self.key=hashlib.sha256(key.encode('utf8')).digest()
    @staticmethod
    def _unpad(s):
        return s[:-ord(s[len(s) - 1:])]
    def decrypt(self, enc):
        iv = enc[:AES.block_size]
        cipher = AES.new(self.key, AES.MODE_CBC, iv)
        return  self._unpad(cipher.decrypt(enc[AES.block_size:]))
    def decrypt_string(self, enc):
        enc = base64.b64decode(enc)
        return  self.decrypt(enc)
        
encrypt = "nhtAabGNL2Gm6A8bblXN0MFuZVHsT/3TU/CnJzyPoimsz7Cx43MGNN3vBpHy6ldhoKHo849JnFVMJmgStDziPAbaY8uojbe1T7uCwY/Dt8rGf294PdehOCK/aLu1FE5TzeToBg+vpBLAOjRD1DvpLDheFrQWlRFicBYa70dy3rCxbpesU/ExKa+6WOBqU73H"
cipher = AESCipher("el4dVTtGPmyZD118VwTPvbmpmiPdRyQq")

# cipher.decrypt_string(encrypt)
print(cipher.decrypt_string(encrypt))

# print("明文:\n{}".format(cipher.decrypt_string(encrypt)))
# 明文:hello world